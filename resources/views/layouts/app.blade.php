<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Blog Seo') }}</title>

    <!-- Styles -->
    <link href="{{ URL:: asset('assets/blog-home/css/blog-home.css') }}" rel="stylesheet">
     <link href="{{ URL::asset('assets/blog-home/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <!--<link href="{{ URL::asset('css/app.css') }}" rel="stylesheet"> -->
</head>

<body>
    <div >
     <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Blog Seo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="{{ url('/') }}">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item" style="border-right: 1px solid white;"></li>
          </ul>
            @if (Route::has('login'))
               <ul  class="navbar-nav">
                     <li class="nav-item" style="border-right: 1px solid white;"></li>
                     <!-- Authentication Links -->
                        @if (Auth::guest())
                            
                            <li class="nav-item"><a class="nav-link"  href="{{ route('login') }}">Login</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                        @else
                            
                            <li class="dropdown nav-item">
                                <a class="nav-link" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li >
                                        <a   href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
               </ul>
            @endif
        </div>
      </div>
    </nav>

        @yield('content')


    </div>

  

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
     <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('assets/blog-home/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/blog-home/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
