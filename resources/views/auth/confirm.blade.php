@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 col-md-offset-8">
            <div class="card mb-4">
                <div class="card-header"></div>

                <div class="card-body">
                    Te hemos enviado un correo con un link para confirmacion de registro,
                    por favor revisa tu correo para que verifiquemos tu correo
                    o registrate con Facebook o Google +
                </div>
            </div>
        </div>
    </div>
</div>
@endsection