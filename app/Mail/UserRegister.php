<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $codeverify;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $codeverify)
    {
        //
        $this->name= $name;
        $this->codeverify= $codeverify;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome')
        ->subject('Bienvenido a Blog Seo')
        ->with(['name' => $this->name,
            'codeverify' => $this->codeverify,
            ])
        /*->with('name', $this->name)*/;
    }
}
