<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Mail\UserRegister as WelcomeUser; //Para el Email
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController as UserC;
use Illuminate\Support\Facades\Mail;
use DB;

class RegisterController extends Controller
{

    protected $redirectTo = 'inactive';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    protected function register(Request $request)
    {

        $userVerify = new UserC;
        //$userVerify->verifyEmail($request->email);

        if($userVerify->verifyEmail($request->email)){

            $code_verify =  str_random(40);

            $usuario= new User;
            $usuario->name = $request->name;
            $usuario->lastname = $request->lastname;
            $usuario->email = $request->email;
            $usuario->full_name = $request->name.' '.$request->lastname;
            $usuario->password=bcrypt($request->password);
            $usuario->profile= 1;
            $usuario->status= 1;
             $usuario->codeverify= $code_verify;
            $usuario->save();

            


       Mail::to($request->email,  $request->name)
            ->send(new WelcomeUser($request->name, $code_verify));
            return view('auth/confirm');

        } else{

            return view('auth/userexists');
        }
    }

     protected function confirm_register($codeverify)
    {

        $userVerify = new UserC;
        //$userVerify->verifyEmail($request->email);

        if($userVerify->verifyCode($codeverify)){

             $user= DB::table('users')
            ->where('codeverify', $codeverify)
            ->update(['status' => 2]);

            return view('auth/login-after-confirm');

        } else{

            return view('auth/userexists');
        }
    }
}
