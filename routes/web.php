<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\UserRegister as WelcomeUser;
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('home');
});

Route::post('register_', 'RegisterController@register')->name('register_');

Route::get('/register/verify/{code}', 'RegisterController@confirm_register');

/*Route::get('/email', function () {
    Mail::to( 'freddy_j_castillo@hotmail.es',  'Freddy')
        ->send(new WelcomeUser('Freddy'));
}); */ 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
