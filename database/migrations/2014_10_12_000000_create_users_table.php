<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblegext_users', function (Blueprint $table) {
            $table->increments('tblegext_id');
            $table->string('tblegext_name');
            $table->string('tblegext_lastname');
            $table->string('tblegext_full_name');
            $table->string('tblegext_email')->unique();
            $table->string('tblegext_password');
            $table->string('tblegext_slugs')->unique();
            $table->string('tblegext_img_user');
            $table->integer('tblegext_profile');
            $table->string('tblegext_phone');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblegext_users');
    }
}
